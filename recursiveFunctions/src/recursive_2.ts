type TArray = number[];
const access = (arr: TArray, i1: number) => {
	for (let i2 = 0; i2 < arr.length; i2++) {
		if (arr[i2] === 0) continue;
		if (arr[i2] === i1)
			return false;
	}
	return true
}

const restartArray = (arr: TArray, index: number) => {
	arr[index] = 0;
	for (let i = arr.findIndex(item => item === 0); i < arr.length; i++) {
		arr[i] = 0;
	}
}

const sum = (arr: TArray): number => {
	return arr.reduce((acc, item) => acc + item, 0);
}

const scan = (arr: TArray): boolean => {
	return arr.every(item => item !== 0);
}

const findIndex = (arr: TArray): number => {
	return arr.findIndex(item => item === 0)
}

const getRandomNum = () => {
	return Math.floor(Math.random() * 9) + 1;
}
//--------------------------------------------------------
const main_1 = () => {
	const total = 20;
	const arrayNumberEquation = (arr: TArray) => {
		console.log("start")
		if (scan(arr) && sum(arr) >= total)
			return true;

		const index = findIndex(arr);

		for (let i1 = 0; i1 < 10; i1++) {
			const num = getRandomNum();
			if (access(arr, num)) {
				arr[index] = num;

				if (sum(arr) <= total && scan(arr))
					return false;

				if (arrayNumberEquation(arr)) {
					console.log("return true");
					return true;
				} else {
					restartArray(arr, index);
				}
			}
		}
		return false;
	}
	const newArray = [0, 0, 0, 0];
	arrayNumberEquation(newArray)
	console.log(newArray)
	console.log(sum(newArray))
}
const main_2 = () => {
	const total = 30;
	const arrayNumberEquation = (arr: TArray) => {
		console.log("start")
		if (scan(arr) && sum(arr) === total)
			return true;

		const index = findIndex(arr);

		for (let i1 = 1; i1 <= 9; i1++) {
			const num = i1;//getRandomNum();
			if (access(arr, num)) {
				arr[index] = num;

				if (sum(arr) < total && scan(arr))
					continue;

				if (arrayNumberEquation(arr)) {
					console.log("return true");
					return true;
				} else {
					restartArray(arr, index);
				}
			}
		}
		return false;
	}
	const newArray = [0, 0, 0, 0];
	arrayNumberEquation(newArray)
	console.log(newArray)
	console.log(sum(newArray))
}

main_2()
