const typeValue = (value: any)=>{
	return Object.prototype.toString.call(value)
}

const arrFlat = ()=>{
	const innerArray: any[] = [];
	const innerFun = (nestedArray: any[])=>{
//--------------------------------------------
		for(let i1 = 0; i1 < nestedArray.length; i1++){
			if(typeValue(nestedArray[i1]).includes("Array")){
				const ass = innerFun(nestedArray[i1]);
				// console.log(ass);
			}else{
				innerArray.push(nestedArray[i1]);
			}
		}
		return innerArray;
//--------------------------------------------
	}
	return innerFun;
}

// const flat = arrFlat();
// console.log(flat(nestedArray));

describe("recursive functions", ()=>{
	const nestedArray = [1, 2, 3, 4, [21, 22, [31, 32, 33], 23], 5, 6, 7, 8, 9];
	test("проверка функции на корректный возврат типа", ()=>{
		expect(typeValue(nestedArray).includes("Array"))
			.toEqual(true);
	});
	test("создать одномерный массив через флат", ()=>{
		expect(arrFlat()(nestedArray))
			.toEqual([1, 2, 3, 4, 21, 22, 31, 32, 33, 23, 5, 6, 7, 8, 9]);
	});
});