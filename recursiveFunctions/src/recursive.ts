const myArray = [1, 2, 3, 4, 5, 6, 666, 7, 8, 9, 10];

const recursiveSum = (arr: number[]) => {
	if (!arr.length)
		return 0;
	const innerArray: number[] = arr.slice(1);
	const accumulator = recursiveSum(innerArray) as number;
	return (accumulator === undefined) ? arr[0] : arr[0] + accumulator;
}
// console.log(recursiveSum(myArray));721
//---------------------------------------------------------------
const recursiveBigNum = (arr: number[]) => {
	if (!arr.length)
		return 0;
	const ss: number[] = arr.slice(1)// as [];

	const innerArray = recursiveBigNum(ss) as number;
	return (innerArray === undefined)
		? arr[0]
		: (arr[0] >= innerArray)
			? arr[0]
			: innerArray;
}
// console.log(recursiveBigNum(myArray))
//---------------------------------------------------------------
type TNumber = number[];
const recursiveReverseArray = (arr: number[]): TNumber => {
	if (!arr.length)
		return arr;

	const innerArray: TNumber = recursiveReverseArray(arr.slice(1));
	return (innerArray === undefined)
		? arr
		: innerArray.concat(arr[0]);
}
// console.log(recursiveReverseArray(myArray))
// [10, 9, 8, 7, 666, 6, 5, 4, 3, 2, 1]
//---------------------------------------------------------------
function recursiveSortArray(arr: number[]): TNumber {
	if (!arr.length)
		return arr;
	let bigNum = arr[0];
	let index = 0;
	for (let i1 = 0; i1 < arr.length; i1++) {
		if (bigNum < arr[i1]) {
			bigNum = arr[i1];
			index = i1;
			// break;
		}
	}

	let concatArr = arr.concat();
	concatArr.splice(index, 1);
	const innerArray = recursiveSortArray(concatArr);

	return (innerArray === undefined)
		? [bigNum]
		: [...innerArray, bigNum];
}

// console.log(recursiveSortArray(myArray));
// console.log(myArray)
//---------------------------------------------------------------
type CurryType = (otherNum: number) => (otherNum: number) => any;
const currySum = (num: number): CurryType => {
	let total = num;
	return <CurryType>function sum(otherNum: number) {
		if (typeof otherNum === "number") {
			total += otherNum;
			return sum;
		}
		return total;
	}
}
// console.log(currySum(1)(2)(3)(4)(5)(6)(7)(8)(9)());
//---------------------------------------------------------------
export {currySum, recursiveSortArray, recursiveReverseArray, recursiveBigNum, recursiveSum, myArray}