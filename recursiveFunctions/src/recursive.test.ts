// https://jestjs.io/ru/docs/expect
import {
	recursiveSortArray,
	recursiveReverseArray,
	recursiveBigNum,
	recursiveSum,
	myArray, currySum,
} from "./recursive"
// const myArray = [1, 2, 3, 4, 5, 6, 666, 7, 8, 9, 10];
describe("recursive functions", () => {
	test("сложение чисел массива", () => {
		expect(recursiveSum(myArray))
			.toBe(721);
	});
	test("самое большое число в массиве", () => {
		expect(recursiveBigNum(myArray))
			.toBe(666);
	});
	test("(пове/разве)рнуть массив", () => {
		expect(recursiveReverseArray(myArray))
			.toEqual(myArray.reverse());
	});
	test("сортировать массив", () => {
		expect(recursiveSortArray(myArray))
			.toEqual([1, 2, 3, 4,  5, 6, 7, 8, 9, 10, 666]);
	});
	test("рекурсивное сложение чисел черех карри функцию", () => {
		expect(currySum(1)(2)(3)(4)(5)(6)(7)(8)(9)())
			.toEqual(45);
	});

	// it("shouldn't change the array length", () => {
	// 	const a = [22, 9, 60, 12, 4, 56];
	// 	shuffle(a);
	// 	expect(a.length).toBe(6);
	// });

	// afterEach(() => {
	// 	// so count of calls to Math.random will be OK
	// 	jest.restoreAllMocks();
	// });

	// beforeEach(() => {
	// 	a = "SORTOFRANDOM".split("");
	// });
});
