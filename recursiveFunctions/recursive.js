/*01*/
if(0){
	function pow(x,n){
		if(n == 1){
			return x;
		}else{
			return x * pow(x,n - 1);
		}
	}

	console.log(pow(2,3));
}
/*02*/
if(0){
	// reverse array
	const arr = [4,8,6,9,5,3,11,2,7,1];
	console.log(reverseArray(arr.toString())); //1,7,2,11,3,5,9,6,8,4

	function reverseArray(arr){
		if(arr.length === 0){
			return null;
		}

		const dd = reverseArray(arr.slice(1));
		if(dd){
			return dd.concat(arr[0])
		}else{
			return arr;
		}
	}
}
/*03*/
if(0){
	/*sorted array using recursion*/
	const arr = [4,8,6,9,5,3,11,2,7,1];
	console.log(sortArray(arr));

//--------------------------------------------------------
	function sortArray(arr){
		if(arr.length === 0)
			return null;

		let bigNum = arr[0];
		let index = 0;
		for(let i1 = 0; i1 < arr.length; i1++){
			if(bigNum < arr[i1]){
				bigNum = arr[i1];
				index = i1;
				// break;
			}
		}

		// console.log(index)
		arr.splice(index,1)
		// console.log(bigNum, "\t", arr);

		const dd = sortArray(arr);
		// console.log(dd, "\t", bigNum);

		if(dd){
			return [...dd,bigNum]
		}else{
			return [bigNum]
		}
	}
}
/*04*/
if(0){
	// найти самое большое число
	const arr = [4,8,16,9,5,3,11,2,7,1];
	console.log(bigNum(arr));

	function bigNum(arr){
		if(!arr.length)
			return 0;

		const firstNum = arr[0];
		const sliceOfArray = arr.slice(1);
		// console.log(firstNum," ",sliceOfArray.toString())
		const currentBigNum = bigNum(sliceOfArray);
		// console.log(firstNum ,"\t", currentBigNum)
		return (firstNum > currentBigNum) ? firstNum : currentBigNum;
	}
}
/*05*/
if(0){
	const arr = [4,8,6,9,5,3,11,2,7,1];
	console.log(sumArr(arr));

	function sumArr(arr){
		if(!arr.length)
			return 0

		const firstNum = arr[0];
		const sliceOfArray = arr.slice(1);
		const currentBigNum = sumArr(sliceOfArray);
		// console.log(firstNum,"\t",currentBigNum);
		return firstNum + currentBigNum
	}
}
/*06*/
if(0){
	console.log(pow(2,3));// 16
	function pow(y,x){
		if(x === 0)
			return 1;

		// console.log(x);
		const totalNum = y * pow(y,x - 1);
		// console.log(totalNum);
		return totalNum;
	}
}
/*07*/
if(0){
	console.log(curry(1)(2)(3)(4)(5)(6)());

	function curry(num){
		let total = num;
		return function sum(otherNum){
			if(typeof otherNum === "number"){
				total += otherNum;
				return sum;
			}
			return total;
		}
	}
}
/*08*/
if(0){
	const strPizza = "tamazA";
	// console.log(strPizza.split("").slice(1).join(""));
	console.log(reverseStr(strPizza));

	function reverseStr(strPizza){
		if(strPizza.length === 0)
			return ""
		const firstPos = strPizza[0]
		const currentResult = reverseStr(strPizza.split("").slice(1).join(""));
		console.log(firstPos," ",currentResult)
		return currentResult.concat(firstPos)
	}
}
/*09*/
if(0){
	// обход и сложение значений веток дерева
	obj1 = {
		right:{
			value:"R",
		},
		left:{
			left:{
				value:"e",
			},
			right:{
				right:{
					value:"c",
				},
				left:{
					right:{
						right:{
							value:"u",
						},
						left:{
							value:"r",
						}
					},
					left:{
						value:"s",
					},
					value:"i",
				},
				value:"o",
			},
			value:"n",
		}
	}
	obj2 = {
		left:{
			value:"R",
			left:{
				value:"e",
				left:{
					value:"c",
					left:{
						value:"u",
						left:{
							value:"r",
							left:{
								value:"s",
								left:{
									value:"i",
									left:{
										value:"o",
										left:{
											value:"n",
										},
									},
								},
							},
						},
					},
				},
			},
		},
	}
	console.log(flatObj(obj2));

//----------------------------------------
	function flatObj(obj){
		console.log("==================");
		let totalSum = "";
		for(const objKey in obj){
			const innerValue = obj[objKey];
			console.log(objKey,"\t",innerValue);
			if(typeof innerValue === "object"){
				const currentValue = flatObj(innerValue);
				totalSum += currentValue;
			}else{
				totalSum += innerValue;
			}
		}
		return totalSum;
	}


}
/*10*/
if(0){
	// глубокое копирование
	obj = {
		right:{
			value:"R",
		},
		left:{
			left:{
				value:"e",
			},
			right:{
				right:{
					value:"c",
				},
				left:{
					right:{
						right:{
							value:"u",
						},
						left:{
							value:"r",
						}
					},
					left:{
						value:"s",
					},
					value:"i",
				},
				value:"o",
			},
			value:"n",
		}
	}
	const newObj = deepClone(obj);
	console.log(JSON.stringify(newObj,null,2))

	//---------------------------
	function deepClone(obj){
		const innerObj = {};
		for(const objKey in obj){
			if(typeof obj[objKey] === "object"){
				const otherObj = deepClone(obj[objKey])
				innerObj[objKey] = otherObj;
			}else{
				innerObj[objKey] = obj[objKey];
			}
		}
		return innerObj;
	}
}
/*11*/
if(0){
	arr = [1,2,[23,[31,34],24,25],3,51,
		52,[523,[5313,51,52,[114,[221,354,35488],
			588,765],646,534],883,500],539];

	const flatArr = (arr)=>{
		const innerArr = [];
		for(const arrElement of arr){
			if(Array.isArray(arrElement)){
				const currentArr = flatArr(arrElement);
				for(const elemArr of currentArr){
					innerArr.push(elemArr);
				}
			}else{
				innerArr.push(arrElement);
			}
		}
		return innerArr;
	}

	console.log(flatArr(arr));
	// console.log(arr.join(",").split(","));
}
/*12*/
if(0){
	// function sum(a,b,c,d){
	// 	return a + b + c + d;
	// }
	//
	// const q1 = sum.bind(null,1,2,3,4);
	// const q2 = sum.call(null,1,2,3,4);
	// const q3 = sum.apply(null,[1,2,3,4]);
	// console.log(typeof q1," ",q1());
	// console.log(typeof q2," ",q2);
	// console.log(typeof q3," ",q3);
//CURRY CURRY CURRY CURRY CURRY CURRY CURRY CURRY CURRY
//CURRY CURRY CURRY CURRY CURRY CURRY CURRY CURRY CURRY
//CURRY CURRY CURRY CURRY CURRY CURRY CURRY CURRY CURRY
//CURRY CURRY CURRY CURRY CURRY CURRY CURRY CURRY CURRY
	if(0){
		function sum(a,b,c,d){
			return a + b + c + d;
		}

		function multi(a,b,c,d){
			return a * b * c * d;
		}

		function curry(callBack){
			console.log(typeof callBack);
//---------------------------------------------
			return function curried(...args){
				console.log(args);
				if(args.length >= callBack.length){
					return callBack.apply(this,args);
				}
				if(0){
					return curried.bind(null,...args);
				}else
					return function test(...newArgs){
						return curried.apply(this,args.concat(newArgs));
					}
			}
//---------------------------------------------
		}

		const curriedSum = curry(sum);
		console.log(curriedSum(1)(2)(3)(4));
	}
	if(0){
		function curry(firstNum){
			let innerValue = firstNum;
			return function sum(otherNums){
				if(typeof otherNums === "number"){
					innerValue += otherNums;
					return sum;
				}else
					return innerValue;
			}
		}

		console.log(curry(1)(2)(3)(4)(5)(6)(7)());
	}
	if(1){
		function add(a,b){

			if(typeof b === 'undefined'){
				return function sum(c){
					if(typeof c === 'undefined')
						return sum;
					else
						return a + c;
				}
			}else
				return a + b;
		}

		console.log(add(20,1)) // -> 21
		console.log(add(20)(2)) // -> 22
		console.log(add(20)(3)) // -> 23
		console.log(add(20)()()()()()()(4)) // -> 24
	}
}
/*13*/
if(0){
	const randomInt = (min,max)=>{
		min = Math.ceil(min);
		max = Math.floor(max);
		return Math.floor(Math.random() * (max - min + 1)) + min;
	}
	/** Для ожидания */
	const delay = (ms)=>{
		return new Promise(resolve=>setTimeout(resolve,ms));
	}

	let counter = 0;

	/** Рекурсивный промис */
	const test = ()=>{
		return new Promise((resolve,reject)=>{
			setTimeout(()=>{
				let random = randomInt(300,1800);
				counter++;
				console.log("Counter:",counter,"Random:",random);
				resolve(test());
			},1000)


			// let random = randomInt(300, 1800);
			// delay(random).then(
			// () => {
			// 	counter++;
			// 	console.log("Counter:", counter, "Random:", random);
			// 	resolve(test());
			// })
		})
	}
	test()
}
/*14*/
if(0){
	const tree = {
		value:2,
		children:[
			{
				value:4,
				children:[
					{
						value:6,
						children:[
							{
								value:8,
								children:[
									{
										value:10,
									}
								]
							},
							{
								value:12,
								children:[
									{
										value:4,
										children:[
											{
												value:6,
												children:[
													{
														value:8,
														children:[
															{
																value:10,
																children:[
																	{
																		value:4,
																		children:[
																			{
																				value:6,
																				children:[
																					{
																						value:8,
																						children:[
																							{
																								value:10,
																							}
																						]
																					},
																					{
																						value:12,
																					}
																				]
																			}
																		]
																	},
																	{
																		value:14,
																	}
																]
															}
														]
													},
													{
														value:12,
													}
												]
											}
										]
									},
									{
										value:14,
									}
								]
							}
						]
					}
				]
			},
			{
				value:14,
			}
		]
	}

	if(0){
		const outerFunc = ()=>{
			let sum = 0;
			const func = (node)=>{
				for(const nodeKey in node){
					if(Array.isArray(node[nodeKey])){
						for(const nodeElementElement of node[nodeKey]){
							func(nodeElementElement);
						}
					}else{
						sum += node[nodeKey];
					}
				}
				return sum;
			}
			return func;
		}
		const ss = outerFunc()
		console.log(ss(tree));
	}
	if(1){
		const func = (node)=>{
			let summ = node.value;
			if(Array.isArray(node.children)){
				node.children.forEach(item=>{
					const innerSum = func(item)
					console.log(innerSum)
					summ += innerSum;
				});
			}
			return summ;
		}
		console.log(func(tree));
	}
	// console.log(Array.isArray([ { value: 2, children: [ [Object] ] }, { value: 10 } ]))
}
/*15*/
if(1){
	const boolTypeOfValue = (value,trigger)=>{
		return Object.prototype.toString.call(value).includes(trigger)
	};
	const obj = {
		A3:{
			// B1:["B3","B17"],
			CD1:{
				C2:{C4:["D4","D5"]}
			},
			EF1:{
				E2:{E4:["F4","F5"]}
			},
			GHI1:{
				G2:{G5:["H5","H6"]}
			},
		},
		A18:{
			B1:"B18",
			EF1:"E19",
			GHI1:{
				H2:["H18","H20","H34"],//определения
				G2:{//заявления и определения
					G22:["H22","H23"],
					G24:["H24","H25"],
					G26:["H26","H27"],
				},
				I2:["I21","I33"]//решения
			}
		},
		A52:{
			EF1:{
				E2:{E52:["F52","F53"]},
			},
			GHI1:{
				H2:"H52",//определения
				G2:{//заявления и определения
					G54:["H54","H55"],
					G56:["H56","H57"],
				},
				// I2:["I21","I33"]//решения
			},
		},
		A67:{
			JK1:{
				J2:"J67"
			}
		}
	}
	
	const getObjPath = (searchingKey,outerObj)=>{
		
		let globalStr = "";
		let shouldStop = false; // Флаг для остановки рекурсии
		/** рекурсия глушимая по условию */
		const traverse = (obj,path = [])=>{
			// console.log(">>> ",JSON.stringify(obj,null,2));
			if(shouldStop) return; // Проверка флага перед продолжением
			
			for(const key in obj){
				if(shouldStop) return; // Проверка флага в цикле
				const innerObj = obj[key];
				
				if(typeof innerObj === 'object' && !Array.isArray(innerObj)){
					traverse(innerObj,path.concat(key));
				}else{
					// console.log(path,"\t\t",key,"\t\t",innerObj);
					
					if(boolTypeOfValue(innerObj,"Array")){
						for(const innerObjElement of innerObj){
							if(innerObjElement === searchingKey){
								globalStr = path.join(" > ").concat(` > ${key} > ${searchingKey}`);
								// console.log(globalStr);
								shouldStop = true;
								return;
							}
						}
					}
				}
			}
			// console.log(">>> ",JSON.stringify(obj,null,2));
		};
		traverse(outerObj);
		return globalStr;
	};
	
	console.log(
		getObjPath("F4",obj)
	)
}
