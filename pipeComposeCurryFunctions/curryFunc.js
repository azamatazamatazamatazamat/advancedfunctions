if(0){
	console.log(curryFunc(1)(2)(3)(4)(5)(6)());

	function curryFunc(n1){
		console.log(">> ",arguments)
		let sum=n1;

		function next(n2){
			console.log(">>> ",arguments)
			if(n2 !== undefined){
				sum+=n2;
				return next
			}else
				return sum;
		}

		return next
	}
}
if(0){
	const curryFunc=(s1,s2)=>new Promise((resolve,reject)=>{
		return setTimeout(()=>{
			resolve(s1 + s2)
		},3000);
	})
	curryFunc(2,9).then(console.log);
}