if(0){
	mainFunc();

	function mainFunc(){
		pipe(
			add("S"),add("P"),add("A"),add("R"),add("T"),tail("A")
		)

		function pipe(...functions){
			console.log(functions[0]("_"));
			console.log(functions[1]("_"));
			console.log(functions[2]("_"));
			console.log(functions[3]("_"));
			console.log(functions[4]("_"));
			console.log(functions[5]);
			let innerStr=""
			for(const function1 of functions){
				innerStr+=typeof function1 === "function" ? function1("_") : function1;
			}
			console.log(innerStr)
		}

		function tail(x){
			return x;
		}

		function add(x){
			return function(y){
				return x + y;
			}
		}
	}
}
if(0){
	mainFunc2();

	function mainFunc2(){
		console.log(
			pipe(
				add("S"),add("P"),add("A"),add("R"),add("T"),tail("A")
			)("_|_")
		);//S_|_P_|_A_|_R_|_T_|_A

		function pipe(...functions){
			return function(item){
				// console.log(item)// _|_
				let innerStr="";
				for(const function1 of functions){
					innerStr+=typeof function1 === "function" ? function1(item) : function1;
				}
				return innerStr;
			}
		}

		function tail(x){
			return x;
		}

		function add(x){
			return function(y){
				return x + y;
			}
		}
	}
}
if(0){
	// const pipe = (...functions) => item => {
	// 	console.log(typeof item)
	// 	return functions.reduce((output, func) => func(output), item);
	// }
//----------------------------------------------------------
// operators
	const pipe=(...functions)=>(item)=>{
		// console.log(typeof item)
		return functions.reduce((output,func)=>func(output),item);
	}
//----------------------------------------------------------
// operators
	const add=(x)=>(y)=>{
		return x + y;
	}
	const multiply=(x)=>(y)=>{
		return x * y
	};
	const subtract=(x)=>(y)=>{
		return x - y
	};
//----------------------------------------------------------

	const calculate=pipe(
		subtract(5)   // 5 - 3 = 2
		,add(20)      // 20 + 2 = 22
		,multiply(2)  // 22 * 2 = 44
	);
	console.log(calculate(3)); // Output: 44
}
if(1){
	// const pipe = (...fns) => (x) => fns.reduce((v, f) => f(v), x)
	const pipe=(...functions)=>(value)=>{

		// console.log(value.name)
		let innerStr=functions[0](value);
		innerStr=functions[1](innerStr);
		innerStr=functions[2](innerStr);
		innerStr=functions[3](innerStr);
		console.log(">> ",innerStr);

		return functions.reduce((currentValue,currentFunction)=>{
			return currentFunction(currentValue);
		// },{name:'azamatik1'});
			},value);
	}
//------------------------------------------------------
	const getName=person=>person.name;
	const uppercase=string=>string.toUpperCase();
	const get_0_to_6_Characters=string=>string.substring(0,6);
	const reverse=(string)=>string.split('').reverse().join('');
//------------------------------------------------------
	const changedString=pipe(
		getName,
		uppercase,
		get_0_to_6_Characters,
		reverse
	)({name:'azamatik'});
	console.log(changedString);
}

