cd C:\Users\azamat.dzhunusov\WebstormProjects\untitled\metanit\advancedFunctions
cd C:\jWEB\projects\metanit\advancedfunctions
cd C:\Projects\metanit\advancedfunctions
//////////////////////////////////////////////////////////////////////////////////////////////////////////
Push an existing folder

cd C:\Users\azamat.dzhunusov\WebstormProjects\untitled\metanit\advancedFunctions
git init --initial-branch=main
git remote add origin git@gitlab.com:azamatazamatazamatazamat/advancedfunctions.git
git add .
git commit -m "Initial commit"
git push --set-upstream origin main
//////////////////////////////////////////////////////////////////////////////////////////////////////////


git checkout -b customFunctions
git add customFunctions/
git commit -m "folder customFunctions has been added"
git push --set-upstream origin customFunctions
git commit -m "добавлена кастомная функция myMap"
git commit -m "переименован файл arrayFunctions.js в myMap.js"
git commit -m "добавлена кастомная функция myFilter"

git checkout -b pipeComposeCurryFunctions
git add pipeComposeCurryFunctions/pipeFunc.js
git commit -m "добавлены примеры работы с pipe функциями"

git checkout pipeComposeCurryFunctions
git add pipeComposeCurryFunctions/curryFunc.js
git commit -m "добавлены примеры работы с curry функциями"

git checkout recursiveFunctions
git checkout -b recursiveFunctions
git add .
git commit -m "добавлены примеры работы с рекурсивными функциями"
git commit -m "добавлен 3 пример работы с рекурсивными функциями"

git fetch origin // стянуть ветку
git checkout -b recursiveFunctions origin/recursiveFunctions // переключиться на ветку

git commit -m "добавлен 4 пример работы с рекурсивными функциями"
git commit -m "добавлен 5 пример работы с рекурсивными функциями"
git commit -m "добавлен 6 пример работы с рекурсивными функциями"
git commit -m "добавлен 7 пример работы с рекурсивными функциями"
git commit -am "добавлен 8 пример работы с рекурсивными функциями"
git commit -am "добавлен 9 пример работы с рекурсивными функциями"
git commit -am "добавлен 10 пример работы с рекурсивными функциями"
git commit -am "добавлен 11 пример работы с рекурсивными функциями"

git commit -m "добавлен пример асинхронной функции myPromise"
git commit -am "добавлены 2 примера для работы с контекстом функции"

