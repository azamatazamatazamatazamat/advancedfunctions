// https://refactoring.guru/ru/design-patterns/builder/typescript/example
import Director from "./allClasses/Director";
import ConcreteBuilder1 from "./allClasses/ConcreteBuilder1";

function clientCode(director: Director) {
	const builder = new ConcreteBuilder1();
	director.setBuilder(builder);

	console.log('Standard basic product:');
	director.buildMinimalViableProduct();
	builder.getProduct().listParts();

	console.log('Standard full featured product:');
	director.buildFullFeaturedProduct();
	builder.getProduct().listParts();

	// Помните, что паттерн Строитель можно использовать без класса Директор.
	console.log('Custom product:');
	builder.producePartA();
	builder.producePartC();
	builder.getProduct().listParts();
}

const director = new Director();
clientCode(director);