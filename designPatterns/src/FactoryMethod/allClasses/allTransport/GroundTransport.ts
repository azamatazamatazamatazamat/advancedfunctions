import Transport from "./Transport";
import {Product} from "../../allTypes";
import RubberDolls from "../allProducts/RubberDolls";
import Dildos from "../allProducts/Dildos";
import Whips from "../allProducts/Whips";
import Lubricants from "../allProducts/Lubricants";

class GroundTransport extends Transport{
	public transportMethod(): Product[]{
		// console.log(typeof new Dildos().operation())
		return [
			new Dildos(),
			new Lubricants(),
			new Whips(),
			new RubberDolls(),
		];
	}
}

export default GroundTransport