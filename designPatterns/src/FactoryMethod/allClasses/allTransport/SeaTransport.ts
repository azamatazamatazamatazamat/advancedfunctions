import Transport from "./Transport";
import {Product} from "../../allTypes";
import Dildos from "../allProducts/Dildos";
import Whips from "../allProducts/Whips";

class SeaTransport extends Transport{
	public transportMethod():Product[]{
		return [new Dildos(),new Whips()];
	}
}

export default SeaTransport;