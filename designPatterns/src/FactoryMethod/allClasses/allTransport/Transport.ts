import {Product} from "../../allTypes";
abstract class Transport{
	public abstract transportMethod(): Product[];

	public displayProducts(): string{
		const innerArray: string[] = [];
		const product = this.transportMethod();
		for(const product1 of product){
			innerArray.push(product1.operation());
		}
		return `Этот транспорт перевозит: ${innerArray.join(", ").trim()}`;
	}
}

export default Transport;