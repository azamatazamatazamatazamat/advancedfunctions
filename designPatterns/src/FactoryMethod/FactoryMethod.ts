import Transport from "./allClasses/allTransport/Transport";
import SeaTransport from "./allClasses/allTransport/SeaTransport";
import GroundTransport from "./allClasses/allTransport/GroundTransport";

function clientCode(kindOfTransport: Transport){
	// ...
	console.log('Клиент: Я не знаю о классе создателя, но он все еще работает.');
	console.log(kindOfTransport.displayProducts());
	// ...
}


clientCode(new GroundTransport());
console.log()
clientCode(new SeaTransport());
