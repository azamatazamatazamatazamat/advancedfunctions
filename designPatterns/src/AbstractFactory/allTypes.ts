interface AbstractFactory{
	createTable(): ITables;
	createChair(): IChair;
	createSofa(): ISofa;
}
interface ITables{infoTable(): string;}
interface IChair{infoChair(): string;}
interface ISofa{
	usefulFunction(): string;
	showChairInfo(collaborator: IChair): string;
	showTableInfo(collaborator: ITables): string;
}

export {AbstractFactory, ISofa, ITables,IChair}