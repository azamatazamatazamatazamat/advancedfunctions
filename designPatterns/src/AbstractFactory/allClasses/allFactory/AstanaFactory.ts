import {AbstractFactory, ITables, ISofa, IChair} from "../../allTypes";
import Table_Avangard from "../allProducts/Table_Avangard";
import Sofa_Avangard from "../allProducts/Sofa_Avangard";
import Chair_Avangard from "../allProducts/Chair_Avangard";
class AstanaFactory implements AbstractFactory {
	public createTable(): ITables {
		return new Table_Avangard();
	}

	public createChair(): IChair {
		return new Chair_Avangard();
	}

	public createSofa(): ISofa {
		return new Sofa_Avangard();
	}
}
export default AstanaFactory;