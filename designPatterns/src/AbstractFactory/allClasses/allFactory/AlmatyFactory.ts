import {AbstractFactory, ITables, ISofa, IChair} from "../../allTypes";
import Table_ArDeco from "../allProducts/Table_ArDeco";
import Sofa_ArDeco from "../allProducts/Sofa_ArDeco";
import Chair_ArDeco from "../allProducts/Chair_ArDeco";

class AlmatyFactory implements AbstractFactory {
	public createTable(): ITables {
		return new Table_ArDeco();
	}
	public createChair(): IChair {
		return new Chair_ArDeco();
	}
	public createSofa(): ISofa {
		return new Sofa_ArDeco();
	}
}
export default AlmatyFactory;