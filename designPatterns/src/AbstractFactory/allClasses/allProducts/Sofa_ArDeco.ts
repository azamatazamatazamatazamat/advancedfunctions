import {ITables, ISofa, IChair} from "../../allTypes";
class Sofa_ArDeco implements ISofa {
	public usefulFunction(): string {
		return 'The result (диван ар-деко)';
	}

	public showTableInfo(collaborator: ITables): string {
		const result = collaborator.infoTable();
		return `The result (${result})`;
	}

	public showChairInfo(collaborator: IChair): string{
		const result = collaborator.infoChair();
		return `The result (${result})`;
	}
}
export default Sofa_ArDeco;