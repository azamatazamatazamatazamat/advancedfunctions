import {ITables, ISofa, IChair} from "../../allTypes";
class Sofa_Avangard implements ISofa {
	public usefulFunction(): string {
		return 'The result (диван авангард)';
	}
	public showChairInfo(collaborator: IChair): string{
		const result = collaborator.infoChair();
		return `The result (${result})`;
	}

	public showTableInfo(collaborator: ITables): string {
		const result = collaborator.infoTable();
		return `The result (${result})`;
	}
}
export default Sofa_Avangard;