import {AbstractFactory} from "./allTypes";
import AstanaFactory from "./allClasses/allFactory/AstanaFactory";
import AlmatyFactory from "./allClasses/allFactory/AlmatyFactory";

function clientCode(factory: AbstractFactory) {
	const table = factory.createTable();
	const chair = factory.createChair();
	const sofa = factory.createSofa();

	console.log(sofa.usefulFunction());
	console.log(sofa.showChairInfo(chair));
	console.log(sofa.showTableInfo(table));
}

console.log('start1');
console.log('start2');
console.log('start3');
clientCode(new AstanaFactory());
console.log();
console.log();
clientCode(new AlmatyFactory());