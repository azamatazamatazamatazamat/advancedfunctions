// map map map map map map map map map map map map map map map map map map map map
// map map map map map map map map map map map map map map map map map map map map
// map map map map map map map map map map map map map map map map map map map map
// map map map map map map map map map map map map map map map map map map map map
Array.prototype.myMap=function(callback1,callback2){
	const self=this;
	if(!Array.isArray(self) || !self.length || typeof callback1 !== 'function'){
		return self;
	}else{
		let result=[];
		for(let i=0; i<self.length; i++){
			if(!callback2(self[i])) continue;
			result.push(callback1(self[i],i,self));
		}
		return result;
	}
}

const callback=(item,index,array)=>{
	return {posit:index + 1,level:item}
}
const filterCallback=(item)=>{
	return item % 4 === 0;
}

const arr=(()=>{
	const innerArray=[];
	for(let i=0; i<100; i++){
		innerArray.push(i);
	}
	return innerArray;
})().myMap(callback,filterCallback);
console.log(arr);

