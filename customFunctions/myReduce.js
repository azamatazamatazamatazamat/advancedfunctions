// reduce reduce reduce reduce reduce reduce reduce reduce reduce reduce reduce
// reduce reduce reduce reduce reduce reduce reduce reduce reduce reduce reduce
// reduce reduce reduce reduce reduce reduce reduce reduce reduce reduce reduce
// reduce reduce reduce reduce reduce reduce reduce reduce reduce reduce reduce

const myArrayObj = [
	{name: "aza1", age: 50}, {name: "saza1", age: 25},
	{name: "aza2", age: 25}, {name: "saza2", age: 20},
	{name: "aza3", age: 25}, {name: "saza3", age: 22},
	{name: "aza4", age: 25}, {name: "saza4", age: 23},
	{name: "aza5", age: 25}, {name: "saza5", age: 24},
]

Array.prototype.myReduce = function(reduceCB, startVal){
	const self=this;
	if(!Array.isArray(self) || !self.length || typeof reduceCB !== 'function'){
		return []
	}else{
		let isValue = startVal !== undefined
		// console.log(isValue,"\t",self[0])
		let value = (isValue) ? startVal : 0;
		for(let i = (isValue) ? 0 : 1; i < self.length; i++){
			value = reduceCB(value, self[i], i, self);
		}
		return value;
	}
}

const arr = myArrayObj.myReduce((sum, item) => {
	sum = sum + item.age;
	return sum;
}, 0);

console.log(arr)