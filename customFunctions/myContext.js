/*1*/if(0){
	const printerObj = {
		name:'TERMINATOR',
		print(greeting = 'hello'){
			console.log(`${greeting}, ${this.name}`);
		}
	};

	if(0){
		// Прямой запуск
		printerObj.print(); // => "hello, TERMINATOR"
		// setTimeout(printer.print, 1000);// => hello, undefined
	}
	if(0){
		setTimeout(
			()=>printerObj.print(),
			4000); //=> "hello, TERMINATOR"
	}
	if(0){
		const value = 'hi';
		setTimeout(()=>printerObj.print(value),1000);
	}
	if(1){
		const boundPrint = printerObj.print.bind(printerObj);
		boundPrint('hi'); // => "hi, TERMINATOR"

		setTimeout(boundPrint,1000);// => "hello, TERMINATOR"
		setTimeout(printerObj.print.bind(printerObj),1000);// => "hello, TERMINATOR"
		// setTimeout(printer.print, 1000);// => hello, undefined
	}
}
/*2*/if(0){
	Function.prototype.varietyDelay = function(){
		const innerFunction = (args)=>{
			console.log(args)
		}
		return innerFunction.bind(this);
	}

	function display(name){
	}

	display.varietyDelay()("Azamat");
}
/*3*/if(0){
	function display(){
		console.log(JSON.stringify(arguments,null,2))
	}

	Function.prototype.delay1000 = function(){
		const DELAY = 1000;
		return function(...args){
			setTimeout(()=>{
				this(...args);
			},DELAY);
		}.bind(this)
	}

	Function.prototype.delay5000 = function(){
		const DELAY = 5000;
		return function(...args){
			setTimeout(()=>{
				this(...args);
			},DELAY);
		}.bind(this)
	}

	Function.prototype.varietyDelay = function(delay){
		return function(...args){
			setTimeout(()=>{
				this(...args);
			},delay);
		}.bind(this)
	}

	const otherFunc = display.delay1000().delay5000();
	otherFunc(1,2,3,4);
}
/*4*/if(1){
	function sum(...args){
		return args.reduce((accumulator,item)=>accumulator + item,0);
	}

	// function curry(callBack){
	// 	return function curried(...args){
	// 		return callBack.apply(null,args);
	// 	}
	// }

	const curry = (callBack)=>(...args)=>{
		return callBack.apply(null,args);
	}

	const curriedSum = curry(sum);
	console.log(curriedSum(1,2,3,4,5));
}