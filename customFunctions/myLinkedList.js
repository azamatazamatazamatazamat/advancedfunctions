// https://www.freecodecamp.org/news/implementing-a-linked-list-in-javascript/
class LinkedList{
	#length=0
	#head
	#tail

	addToTail(value){
		const node={
			value:value,
			next:null
		}
		if(this.#length === 0){
			this.#head=node;
			this.#tail=node;
		}else{
			this.#tail=node;
		}
		this.#length++;
	}

	removeFromHead(){
		if(this.#length === 0){
			return
		}

		const value=this.#head.value;
		this.#head=this.#head.next;
		this.#length--;
		return value;
	}

	get size(){
		console.log(this.#head.value)

		return this.#length;
	}

	get display(){
		return this.#head;
	}
}

//------------------------------------------------------------
class Queue extends LinkedList{
	#storage={};
	constructor(){
		super();
		this.enqueue=this.addToTail;
		this.dequeue=this.removeFromHead;
	}

	get size(){
		return super.size;
	}

	get display2(){
		return super.display;
	}
}
//------------------------------------------------------------
const queue=new Queue();
queue.enqueue(1);
queue.enqueue(2);
queue.enqueue(3);
queue.enqueue(4);
queue.enqueue(5);

console.log(queue.size)
console.log(queue.display2)


