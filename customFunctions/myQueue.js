class Queue{
	#storage={};
	#last=0;
	#first=0;

	enqueue(item){
		this.#storage[this.#last]=item;
		this.#last++;
	}

	dequeue(){
		if(this.size === 0){
			return;
		}
		const value=this.#storage[this.#first];
		delete this.#storage[this.#first];
		this.#first++;
		return value;
	}

	get display(){
		console.log(this.#storage)
	}

	get size(){
		return this.#last - this.#first
	}
}

const table=new Queue();
table.enqueue("aza1");
table.enqueue("aza2");
table.enqueue("aza3");
table.enqueue("aza4");
table.enqueue("aza5");
console.log(table.size)
table.display
///////////////////////////////////////////
table.dequeue();
table.dequeue();
console.log(table.size)
table.display
///////////////////////////////////////////
