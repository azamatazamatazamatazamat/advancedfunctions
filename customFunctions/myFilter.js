// filter filter filter filter filter filter filter filter filter filter filter
// filter filter filter filter filter filter filter filter filter filter filter
// filter filter filter filter filter filter filter filter filter filter filter
// filter filter filter filter filter filter filter filter filter filter filter
const myArrayObj=[
	{name:"xxx1",age:50},{name:"rrr1",age:25},{name:"yyy1",age:25},
	{name:"xxx2",age:25},{name:"yyy2",age:21},{name:"rrr2",age:21},
	{name:"rrr3",age:22},{name:"xxx3",age:25},{name:"yyy3",age:22},
	{name:"xxx4",age:25},{name:"rrr4",age:23},{name:"yyy4",age:23},
	{name:"xxx5",age:25},{name:"rrr5",age:24},{name:"yyy5",age:24},
];

Array.prototype.myFilter=function(filterCallback){
	const self=this;
	if(!Array.isArray(self) || !self.length || typeof filterCallback !== 'function'){
		return [];
	}else{
		let result=[];
		for(let i=0,len=self.length; i<len; i++){
			if(filterCallback(self[i],i,self)){
				result.push(self[i]);
			}
		}
		return result;
	}
}
const arr=myArrayObj.myFilter(item=>/^[^x]{3}\d$/.test(item.name));
console.log(arr);
/*
[
  { name: 'rrr1', age: 25 },
  { name: 'yyy1', age: 25 },
  { name: 'yyy2', age: 21 },
  { name: 'rrr2', age: 21 },
  { name: 'rrr3', age: 22 },
  { name: 'yyy3', age: 22 },
  { name: 'rrr4', age: 23 },
  { name: 'yyy4', age: 23 },
  { name: 'rrr5', age: 24 },
  { name: 'yyy5', age: 24 }
]
*/