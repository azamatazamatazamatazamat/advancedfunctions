"use strict"

class MyPromise{
	static status = ["PENDING","FULFILLED","REJECTED"];
	#value;
	#handlersArray;
	#errorFunc;
	#state;

	constructor(callback){
		this.#value = null;
		this.#handlersArray = [];
		this.#errorFunc = null;
		this.#state = MyPromise.status[this.findIndex("PENDING")];

		this.execute = this.execute.bind(this);
		this.resolveForCallBack(callback, this.resolve.bind(this), this.reject.bind(this));
	}

	findIndex(state){
		const index = MyPromise.status.findIndex(item => item === state)
		return index;
	}

	catch(onRejected){
		if(this.#state === MyPromise.status[this.findIndex("REJECTED")]){
			onRejected(this.#value);
		}else if(this.#state === MyPromise.status[this.findIndex("PENDING")]){
			this.#errorFunc = onRejected;
		}
	}

	fulfilled(result){
		this.#state = MyPromise.status[this.findIndex("FULFILLED")];
		this.#value = result;
		this.#handlersArray.forEach(this.execute);
		this.#handlersArray = [];
	}

	then(onFulFilled, onRejected){
		return new MyPromise((resolve, reject) => {
			const objHandle = {
				onFulFilled: function(result){
					try{
						const innerOnFulFilled = onFulFilled(result);
						// console.log(innerOnFulFilled," <<<",result)
						return resolve(innerOnFulFilled);
					}catch(e){
						return reject(e);
					}
				},
				onRejected: function(err){
					try{
						const innerOnRejected = onRejected(err);
						console.log(innerOnRejected," <<<",err)
						return resolve(innerOnRejected);
					}catch(e){
						return reject(e);
					}
				}
			}
			return this.execute(objHandle);
		});
	}

	execute(objHandle){
		switch(this.#state){
			case "PENDING": this.#handlersArray.push(objHandle);
				break;
			case "FULFILLED":
			case "REJECTED": objHandle.onFulFilled(this.#value);
				break;
		}
	}

	resolveForCallBack(callback,onResolve,onRejected){
		callback(
			function(value){ onResolve(value); },
			function(value){ onRejected(value); }
		);
	}

	resolve(result){
		try{
			this.fulfilled(result);
		}catch(e){
			this.reject(e)
		}
	}

	reject(error){
		this.#state = MyPromise.status[this.findIndex("REJECTED")];
		this.#value = error;
		this.#handlersArray.forEach(this.execute);
		this.#handlersArray = new Array();
		if(this.#errorFunc)
			this.#errorFunc(this.#value)
	}
}
// test1 ---------------------------------------------------------------------
const test1 = new MyPromise(function(resolve,reject) {
	setTimeout(function(){
		resolve(124);
	},1000);
});

test1
	.then(function(data){
		return data + 20
	})
	.then(function(data){
		return data * 2
	})
	.then(function(data){
		return data / 2
	})
	.then(function(data){
		return data - 44
	})
	.then(function(data){console.log("test1", data)});

// test2 ---------------------------------------------------------------------
const test2 = new MyPromise(function(resolve,reject){
	setTimeout(function(){
		reject('error');
	},1000);
});
test2.catch(err => console.log("test2", err));
