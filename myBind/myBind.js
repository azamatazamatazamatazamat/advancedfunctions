//https://badtry.net/kak-priviazat-mietody-klassa-k-ekziempliaru-klassa-s-kontiekstom-this/
/*1*/
if(0){
	class Logger{
		constructor(){
			this.printName = this.printName.bind(this);
		}

		printName(name = 'some log message'){
			this.print(`Debug: ${name}`);
		}

		print(text){
			console.log(text);
		}
	}

	const logger = new Logger();
	const {printName} = logger;
	printName();
}
/*2*/
if(0){
	function mul(...arg){
		console.log(arg.length)
		return arg.reduce((acc,item)=>acc + item,0);
	}

	let double = mul.bind(null,2)
		.bind(null,3)
		.bind(null,4)
		.bind(null,5)
		.bind(null,6)
	console.log(double()); // = mul(2, 3) = 6
}
/*3*/
if(0){

	function myFun([name,arr]){
		console.log(name)
		console.log(arr)
		// return arg.reduce((acc,item)=>acc+item,0);
	}
	const arr = [1,2,3,4,5,6,7,8];
	// let otherFunction=myFun.bind(null,["aza",arr])
	// otherFunction()
	myFun.bind(null,["aza",arr])()
}
/*4*/
if(0){
	const factorial = function f(n){
		return (n !== 1)
			?f(n - 1) + n
			:n;
	}
	if(0){
		function func1(func){
			return function(){
				return func.apply(this,arguments);
			}
		}
		console.log(func1(factorial)(4))
	}
	if(1){
		function func2(func){
			const cache = {}
			console.log(cache)
			return function(n){
				if(typeof cache[n] === "undefined" )
					cache[n] = func.apply(this,arguments)
				return cache[n]
			}
		}
		console.log(func2(factorial)(4))
		console.log(func2(factorial)(10))
	}
}
/*5*/
if(0){
	/** 5 example*/
	function twoArg(arg1,arg2){
		console.log(arg1,"+",arg2);
	}

	const ss1 = twoArg.bind(null,"Aza")
	const ss2 = ss1.bind(null,"mat")
	ss2()
}
/*6*/
if(0){
	const user = {
		name:'Alice',
		sayHello:function(){
			console.log(`Hello, my name is ${this.name}`);
		}
	};

	user.sayHello.bind(user)()
}
/*7*/
if(0){
	class Counter{
		constructor(){
			this.count = 1;
			this.increment = this.increment.bind(this); // Привязываем increment к текущему экземпляру Counter
		}
		increment(){
			this.count++;
			console.log('Current count:',this.count);
		}
	}
	const counter = new Counter();
	setTimeout(counter.increment, 1000);
}
/*8*/
if(1){
	class Counter2{
		// count = 0;
		constructor(){
			this.count = 1;
			// this.increment = this.increment.bind(this); // Привязываем increment к текущему экземпляру Counter2
		}
		increment(){
			this.count++;
			console.log('Current count:',this.count);
		}
	}
	const counter = new Counter2();
	setTimeout(counter.increment.bind(counter), 1000);
}