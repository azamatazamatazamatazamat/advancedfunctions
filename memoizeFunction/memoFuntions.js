//https://www.sitepoint.com/implementing-memoization-in-javascript/
//https://inlehmansterms.net/2015/03/01/javascript-memoization/
/*1*/
if(0){
	const memoFibonacci = (()=>{
		const innerArray = [1,1];
		return (n)=>{
			if(innerArray.length >= n){
				return innerArray.slice(0,n);
			}
			while(innerArray.length < n){
				innerArray.push(innerArray.at(-2) + innerArray.at(-1));
			}
			return innerArray
		}
	})();

	console.log(memoFibonacci(8));
	console.log(memoFibonacci(2));
	console.log(memoFibonacci(10));
	console.log(memoFibonacci(4));
}
/*2*/
if(0){
	const add = (n)=>(m)=>n + m;
	console.log('Simple call',add(3));

	const memoize = (fn)=>{
		let cache = {};
		return (...args)=>{
			let n = args[0];
			if(n in cache){
				console.log('Извлечение с кэша ' + cache[n]);
				return cache[n];
			}else{
				console.log('вычисление результата');
				let result = fn(n)(10);
				cache[n] = result;
				return result;
			}
		}
	}
// creating a memoized function for the 'add' pure function
	const memoizedAdd = memoize(add);

	console.log(memoizedAdd(3));  // calculated
	console.log(memoizedAdd(3));  // cached
	console.log("--------------------------")
	console.log(memoizedAdd(5));  // calculated
	console.log(memoizedAdd(5));  // cached
}
/*3*/
if(0){
	// Мемоизированная рекурсивная функция.
	// та же функция, что была и до этой функции.
	const memoize = (fn)=>{
		let cache = {}
		return (...args)=>{
			let n = args[0];

			if(n in cache){
				console.log('Извлечение с кэша ' + n);
				console.log(cache);
				return cache[n];
			}else{
				console.log('вычисление результата ' + n);
				let result = fn(n);
				cache[n] = result;
				return result;
			}
		}
	}

	const callBack = (x)=>{
		if(x === 0){
			return 1;
		}else{
			let f1 = factorial(x - 1);
			console.log(x," _ ",f1);
			return x * f1;
		}
	}

	const factorial = memoize(callBack);
	console.log(">>> ",factorial(5)); // вычисление
	console.log(">>> ",factorial(6)); // вычисление для 6 и извлечение для 5
	// console.log(factorial(3)); // вычисление для 6 и извлечение для 5
}
/*4*/
if(0){
	function memoize(func){
		let acc = 0;

		return function(returnResult,num = 0){
			if(returnResult){
				console.log("-----------------------------------------")
				console.log('Возвращаем закешированный результат');
				return acc;
			}else{
				const result = func(num);
				switch(result){
					case "plus":
						acc += num;
						break;
					case "minus":
						acc -= num;
						break;
				}

				return `текущий результат сложения: ${acc}`;
			}
		};
	}

//-----------------------------------------------------------------
	const plus = (num)=>{
		console.log("operand plus: " + num);
		return "plus";
	}
	const minus = (num)=>{
		console.log("operand minus: " + num);
		return "minus";
	}

	const memoizedAdd = memoize(plus);
	console.log(memoizedAdd(false,2));
	console.log(memoizedAdd(false,3));
	console.log(memoizedAdd(false,5));
	console.log(memoizedAdd(false,10));
	console.log(memoizedAdd(true));
}
/*5*/
if(0){
	const fStr = ()=>{
		return "Куанышов Ерлан Куанышович\n" +
			"г.Кызылорда, ул. А.Тоқмағанбетов, 36 А\n" +
			"+7 (778) 671-11-11\n" +
			"kuanyshov.e@mail.ru\n" +
			"\n" +
			"Болтаеву Еркебулану Бекайдаровичу\n" +
			"город Тараз, улица Карахана дом 2\n" +
			"+7 (771) 332-92-22\n" +
			"boltaev-e@mail.ru\n" +
			"\n" +
			"\n" +
			"Курмашеву Арману Темирболатовичу\n" +
			"г. Павлодар, ул. Машхур Жусуп 189,  кабинет 310,\n" +
			"8 701  444 28 45\n" +
			"kurmashev29071982@mail.ru\n" +
			"\n" +
			"\n" +
			"Курмашев Арман Темирболатович\n" +
			"г. Павлодар, ул. Машхур Жусуп 189,  кабинет 310\n" +
			"+77014442845\n" +
			"kurmashev29071982@mail.ru\n" +
			"\n" +
			"\n" +
			"Жилкайдарову Самату Бимагамбетовичу\n" +
			"г.Актобе, ул. Маресьева 105, офис 307\n" +
			"+7 (777) 167-79-84\n" +
			"samat_79@List.ru\n" +
			"\n" +
			"\n" +
			"Куанышов Ерлан Куанышович\n" +
			"г.Кызылорда, ул. А.Тоқмағанбетов, 36 А\n" +
			"+7 (778) 671-11-11\n" +
			"kuanyshov.e@mail.ru\n" +
			"\n" +
			"\n" +
			"Турушеву Ринату Мухаметкаировичу\n" +
			"г. Астана, пр. Кабанбай батыра, 13, оф. 20\n" +
			"8 778 364 8680\n" +
			"turushev.sudebnik@mail.ru\n" +
			"\n" +
			"\n" +
			"Нигметулы Фархад\n" +
			"040010\tг. Талдыкорган, ул. Желтоксан 222 А\n" +
			"87773676992\n" +
			"chsi.taldyk@gmail.com\n" +
			"\n" +
			"\n" +
			"Бейсекеевой Алие Жетибайкызы\n" +
			"г.Талгар, ул.Гагарина 92, оф.16\n" +
			"87051618756\n" +
			"a.beysekeeva_sud@mail.ru\n" +
			"\n" +
			"\n" +
			"Қуанышов Ерлан Қуанышұлы\n" +
			"г.Кызылорда, ул. А.Тоқмағанбетов, 36 А\n" +
			"+7 (778) 671-11-11\n" +
			"kuanyshov.e@mail.ru\n" +
			"\n" +
			"\n" +
			"Колбаеву Айбеку Сарсебековичу\n" +
			"г.Шымкент,ул. Рыскулова №1775 здание \"ЛЕДИ-М\" 3-этаж, 7-кабинет.\n" +
			"WhatsApp 8 775 568 81 61: 8 702 679 77 14 +77021710031\n" +
			"87021710031@mail.ru\n" +
			"\n" +
			"\n" +
			"Юсупову Толкуну Исламкуловичу\n" +
			"Алматинская обл, г.Каскелен, ул. Абылайхана 221 Б, офис 4,\n" +
			"+87777913120\n" +
			"chsi.yusupov.timur@mail.ru\n" +
			"\n" +
			"\n" +
			"Колбаеву Айбеку Сарсебековичу\n" +
			"г.Шымкент,ул. Рыскулова №1775 здание \"ЛЕДИ-М\" 3-этаж, 7-кабинет.                                                                                                             WhatsApp 8 775 568 81 61: 8 702 679 77 14 +77021710031 87021710031@mail.ru\n" +
			"87755688161 87026797714 +77021710031\n" +
			"87021710031@mail.ru\n" +
			"\n" +
			"\n" +
			"Далабаеву Дархану Сабуртановичу\n" +
			"г. Караганда, ул. Бухар-Жырау 24,\n" +
			"+7 (702) 444-29-32\n" +
			"chsi_kz@mail.ru\n" +
			"\n" +
			"\n" +
			"Бичуинов Сериккан Кажекенович\n" +
			"РК, ВКО,Г.Семей, ул.Жамакаева, 138\n" +
			"+77771520000, +77027772185\n" +
			"chsi.bichuinov@mail.ru\n" +
			"\n" +
			"\n" +
			"Елеусизов Дамир Кумарканович\n" +
			"ВКО, г. Усть-Каменогорск, ул. Максима Горького 57 офис 111\n" +
			"77776692626\n" +
			"damir.eleusizov.89@mail.ru\n" +
			"\n" +
			"\n" +
			"Жилкайдарову Самату Бимагамбетовичу\n" +
			"г.Актобе, ул. Маресьева 105, офис 307\n" +
			"+7 (777) 167-79-84\n" +
			"samat_79@List.ru\n" +
			"\n" +
			"\n" +
			"Мырзабаев Талгат\n" +
			"РК, Усть-Каменогорск, Максима Горького 72 оф 6\n" +
			"+77779923365\n" +
			"mr.talgat75@mail.ru\n" +
			"\n" +
			"\n" +
			"Бейсекеевой Алие Жетибайкызы\n" +
			"г.Талгар, ул.Гагарина 92, оф.16\n" +
			"87051618756\n" +
			"a.beysekeeva_sud@mail.ru\n" +
			"\n" +
			"\n" +
			"Жакыпбек Эльвира Жакыпбеккызы\n" +
			"г.Алматы, ул.Жандосова, д. 51, офис 719\n" +
			"87026329655\n" +
			"elviraa85@mail.ru\n" +
			"\n" +
			"\n" +
			"Ибрашеву Бауржану Нурлановичу\n" +
			"Адрес: г.Уральск, пр.Н.Назарбаева 215, 2-этаж, 5-кабинет.\n" +
			"87014204251\n" +
			"baur190@mail.ru\n" +
			"\n" +
			"\n" +
			"Турушеву Ринату Мухаметкаировичу\n" +
			"г. Астана, пр. Кабанбай батыра, 13, оф. 20\n" +
			"8 778 364 8680\n" +
			"turushev.sudebnik@mail.ru\n" +
			"\n" +
			"\n" +
			"Юсупову Толкуну Исламкуловичу\n" +
			"Алматинская обл, г.Каскелен, ул. Абылайхана 221 Б, офис 4,\n" +
			"+87777913120\n" +
			"chsi.yusupov.timur@mail.ru\n" +
			"\n" +
			"\n" +
			"Туреханову Ерлану Орынбасаровичу\n" +
			"Туркестанская область, Толебийский район, г. Легнер, улица Толеби, б/н\n" +
			"+7 702 653 22 20\n" +
			"x_088_teo@bk.ru\n" +
			"\n" +
			"\n" +
			"Бейсекеевой Алие Жетибайкызы\n" +
			"г.Талгар, ул.Гагарина 92, оф.16\n" +
			"87051618756\n" +
			"a.beysekeeva_sud@mail.ru\n" +
			"\n" +
			"\n" +
			"Болтаеву Еркебулану Бекайдаровичу\n" +
			"город Тараз, улица Карахана дом 2\n" +
			"+7 (771) 332-92-22\n" +
			"boltaev-e@mail.ru\n" +
			"\n" +
			"\n" +
			"Қуанышов Ерлан Қуанышұлы\n" +
			"г.Кызылорда, ул. А.Тоқмағанбетов, 36 А\n" +
			"+7 (778) 671-11-11\n" +
			"kuanyshov.e@mail.ru\n" +
			"\n" +
			"\n" +
			"Абдикулову Батырбеку Булатовичу\n" +
			"060001\tАтырау, ул.Баймуханова 58/2, 2 этаж 23 кабинет\n" +
			"87078909433 87782487426\n" +
			"b.abdikulov@bk.ru\n"
	}

	const newArray = fStr().split("\n").map(item=>{
		return item ? item : null;
	}).filter(item=>item);


	const memoFunction = ()=>{
		//----------------------------------------------
		let mainArray = (()=>{
			const innerArray = [];
			for(let i = 0; i < newArray.length; i = i + 4){
				innerArray.push(newArray[i + 3]);
			}
			/** delete duplicates*/
			return innerArray.reduce((array,item)=>{
				if(!array.includes(item))
					array.push(item);
				return array;
			},[]);
		})();
		//----------------------------------------------
		return {
			// property: 'Hello',
			showResult:()=>{
				console.log(mainArray)
			},
			computeResult:(email = "")=>{
				mainArray = mainArray.filter(item=>item !== email);
				return mainArray;
			},
			myMethod:function(){
				const arrowFunction = ()=>{
					this.showResult();
				};
				arrowFunction();
			}
		}
	}
//--------------------------------------------------------------------
	const memoized = memoFunction();
	memoized["showResult"]();
	memoized["computeResult"]('kuanyshov.e@mail.ru');
	memoized["computeResult"]('boltaev-e@mail.ru');
	memoized["computeResult"]('kurmashev29071982@mail.ru');
// memoized["showResult"]();
	memoized["myMethod"]();
}
/*6*/
if(1){
	const memoFunction = (setLength)=>{
		let mainArray = (()=>{
			const arr = new Array(setLength);
			return arr.fill(1).map((item,index)=>{
				return item + index;
			})
		})();

		return {
			showResult:(message = "")=>{
				console.log("["+mainArray + "] " + message)
			},
			add:(value = "")=>{
				mainArray.push(value);
			},
			deleteAll:(value)=>{
				mainArray = mainArray.filter(item=>item !== value);
			},
			deleteFirst:(value)=>{
				const index = mainArray.findIndex(item=>item === value);
				if(index >= 0)
					mainArray.splice(index,1);
			},
			myMethod:function(){
				const arrowFunction = ()=>{
					this.showResult();
				};
				arrowFunction();
			}
		}
	}
//--------------------------------------------------------------------
	const memoized = memoFunction(10);
	memoized["showResult"]("начальный массив");
	memoized["add"](16);
	memoized["add"](17);
	memoized["showResult"]("добавили 16 и 17");
	memoized["add"](19);
	memoized["add"](19);
	memoized["add"](19);
	memoized["add"](19);
	memoized["add"](19);
	memoized["showResult"]("добавили 19 - пять раз");
	memoized["deleteAll"](19);
	memoized["showResult"]("удалили все числа 19");
	memoized["add"](19);
	memoized["add"](19);
	memoized["add"](19);
	memoized["showResult"]("добавили 19 - три раза");
	memoized["deleteFirst"](19);
	memoized["showResult"]("удалили первое попавшееся число 19 - один раз");

// 	memoized["myMethod"]();
}